# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.2.0] - 2020-06-13 (YYYY-MM-DD)

### Added
- Added typescript
- Added Static Code Analysis plugins
- Added morgan
- Added `JEST` library for unit testing
- Added unit test cases

### Changed
- `npm` is removed and `yarn` is introduced
- Controllers added for parsing request and generate response
- Generic `POST` response message template added

### Removed
- `npm` is removed
- `javacript` removed and `typescript` added
- build.sh


## [0.1.0] - 2020-04-28 (YYYY-MM-DD)

### Added

- Created basic folder structure
- Dockerfile added as per nodejs parameters
- Project directory initialized
- Pod specs added for skaffold
- LICENSE, README, CHANGELOG added



# Reference
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).