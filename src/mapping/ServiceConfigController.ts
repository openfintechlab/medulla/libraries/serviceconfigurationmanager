import OracleDBInteractionUtil          from "..//utils/OracleDBInteractionUtil";
import util                             from "util";
import { v4 as uuidv4 }                 from "uuid";
import  logger                          from "../utils/Logger";
import {PostRespBusinessObjects}        from "./bussObj/Response";
import AppConfig                        from "..//config/AppConfig";
import { version } from "os";

export class ServiceConfigurationController{

    SQLStatements = {
        "SELECT_SQL01_GETON_SRV_VER_ID": `SELECT service_id,service_name, version_id, config_key,config_key_desc,config_value,config_type,config_hash,created_on,updated_on,created_by  FROM med_service_config WHERE (service_id = :v1 or service_name = :v2) AND version_id = :v3 order by created_on desc`,
        "SELECT_SQL02_GETON_SRV_VER_CFGKEY_ID": `SELECT service_id, version_id, config_key FROM med_service_config WHERE service_id = :v1 AND version_id = :v2 AND config_key=:v3`,
        "INSERT_SQL03_INTO_MEDSERVICE": `INSERT INTO med_service_config (service_id, service_name,version_id,config_key,config_key_desc,config_value,config_type,config_hash,created_on,updated_on,created_by) VALUES (:service_id, :service_name,:version_id,:config_key,:config_key_desc,:config_value,:config_type,:config_hash,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'SYSTEM')`,
        "DELETE_SQL04_DELETE_MEDSERVICE_CONFIG": `DELETE FROM med_service_config where (service_id = :v0 AND version_id = :v1 AND config_key = coalesce(:v2,config_key))`,
        "UPDATE_SQL_05_UPDATESRVCONFIG": `UPDATE med_service_config set service_name=:v0, config_key_desc=:v1,config_value=:v2,config_type=:v3,config_hash=:v4 where service_id=:v5 AND version_id=:v6 AND config_key=:v7`
    }

    /**
     * Get all service configuration based on the service_id and version_id
     * @param {string} serviceID Service ID to fetch the service configuration for
     * @param {string} versionID To fetch the service configuration for
     * @async
     */
    public async getAll(serviceID:string, versionID:string, serviceName?:string){
        logger.info(`Getting service configuration based on serviceID: ${serviceID} and versionID: ${versionID}`);
        let binds = [serviceID, serviceName, versionID];
        try{
            let result = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL01_GETON_SRV_VER_ID,binds);
            logger.debug(`Result of DBSelect for serviceID: ${serviceID} and versionID: ${versionID} : ${result}`);
            let busObj = await this.createBusinessObjectForAllConfigs(result);            
            return busObj;
        }catch(error){
            logger.error(`Error occured while converting data to business object ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }        

    }

    /**
     * Get all service configuration based on the service_id and version_id in slim format
     * @param {string} serviceID Service ID to fetch the service configuration for
     * @param {string} versionID To fetch the service configuration for
     * @async
     */
    public async getSlim(serviceID:string, versionID:string, serviceName?:string){
        logger.info(`Getting service configuration based on serviceID: ${serviceID} and versionID: ${versionID}`);
        let binds = [serviceID, serviceName, versionID];
        try{
            let result = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL01_GETON_SRV_VER_ID,binds);
            logger.debug(`Result of DBSelect for serviceID: ${serviceID} and versionID: ${versionID} : ${result}`);
            let busObj = await this.createBussObjForAllConfigs_Slim(result);            
            return busObj;
        }catch(error){
            logger.error(`Error occured while converting data to business object ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }        

    }

    public async create(request:any){
        logger.debug(`Request Received for creating the service-configuration: ${util.inspect(request,{compact:true,colors:true, depth: null})}`);        
        try{            
            let insertBinds:any[] = [];
            request.serviceconfiguration.config.forEach((config:any) => {
                insertBinds.push([
                    request.serviceconfiguration.service_id,
                    request.serviceconfiguration.service_name,
                    request.serviceconfiguration.version_id,
                    config.config_key,
                    config.config_key_desc,
                    config.config_value,
                    config.config_type,
                    '9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08'

                ])
            });            
            let dbResult = await OracleDBInteractionUtil.executeBatchUpdate(this.SQLStatements.INSERT_SQL03_INTO_MEDSERVICE,insertBinds);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!", new PostRespBusinessObjects.Trace('rowsAffected',`${dbResult.rowsAffected}`));

        }catch(error){
            logger.error(`Error occured while converting data to business object ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }   
    }

    public async delete(serviceID: string, versionID:string, configKey?:string){
        logger.debug(`Request Received for deleting service-configuration: serviceID-${serviceID} versionID-${versionID} configKey-${configKey} `);
        try{            
            let binds = [serviceID,versionID,configKey];
            let dbResult = await OracleDBInteractionUtil.executeUpdate(this.SQLStatements.DELETE_SQL04_DELETE_MEDSERVICE_CONFIG,binds);
            if(dbResult.rowsAffected<=0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8501","Required keys does not exist in the db", 
                            new PostRespBusinessObjects.Trace('rowsAffected',`${dbResult.rowsAffected}`));
            }
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!", new PostRespBusinessObjects.Trace('rowsAffected',`${dbResult.rowsAffected}`));
        }catch(error){
            logger.error(`Error occured deleting records ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }   
    }

    public async update(request:any){
        logger.debug(`Request Received for updating the service-configuration: ${util.inspect(request,{compact:true,colors:true, depth: null})}`);        
        try{            
            let binds:any[] = [];
            request.serviceconfiguration.config.forEach((config:any) => {
                binds.push([                    
                    request.serviceconfiguration.service_name,
                    config.config_key_desc,
                    config.config_value,
                    config.config_type,
                    '9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08',
                    request.serviceconfiguration.service_id,
                    request.serviceconfiguration.version_id,
                    config.config_key

                ])
            });            
            let dbResult = await OracleDBInteractionUtil.executeBatchUpdate(this.SQLStatements.UPDATE_SQL_05_UPDATESRVCONFIG,binds);
            if(dbResult.rowsAffected<=0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8501","Required keys does not exist in the db", 
                            new PostRespBusinessObjects.Trace('rowsAffected',`${dbResult.rowsAffected}`));
            }
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!", new PostRespBusinessObjects.Trace('rowsAffected',`${dbResult.rowsAffected}`));

        }catch(error){
            logger.error(`Error occured while converting data to business object ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }   
    }
    


    private async createBussObjForAllConfigs_Slim(dbResult:any){
        return new Promise<any> ((resolve:any, reject: any) => {
            if(dbResult.rows.length <= 0){
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8501","Required key does not exist in the db"));
            }
            
            var busObj:any = {
                metadata: {
                    "status": '0000',
                    "description": 'Success!',
                    "responseTime": new Date(),
                    
                },
                [dbResult.rows[0].SERVICE_NAME]: {}                
            }

            for(let i:number = 0; i< dbResult.rows.length; i++){
                // var config[dbResult.rows[i].CONFIG_KEY] = dbResult.rows[i].CONFIG_VALUE;             

                busObj[dbResult.rows[0].SERVICE_NAME][dbResult.rows[i].CONFIG_KEY] = dbResult.rows[i].CONFIG_VALUE ;              
            }

            resolve(busObj);
        });
    }


    private async createBusinessObjectForAllConfigs(dbResult:any){
        return new Promise<any> ((resolve:any, reject: any) => {
            if(dbResult.rows.length <= 0){
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8501","Required key does not exist in the db"));
            }
            
            var busObj:any = {
                metadata: {
                    "status": '0000',
                    "description": 'Success!',
                    "responseTime": new Date(),
                    
                },
                "serviceconfiguration":{
                    "service_id":       dbResult.rows[0].SERVICE_ID,
                    "service_name":     dbResult.rows[0].SERVICE_NAME,
                    "version_id":       dbResult.rows[0].VERSION_ID,
                    "config":[]
                }
            }

            for(let i:number = 0; i< dbResult.rows.length; i++){
                busObj.serviceconfiguration.config.push(
                    {                        
                        "config_key":       dbResult.rows[i].CONFIG_KEY,
                        "config_key_desc":  dbResult.rows[i].CONFIG_KEY_DESC,
                        "config_value":     dbResult.rows[i].CONFIG_VALUE,
                        "config_type":      dbResult.rows[i].CONFIG_TYPE,
                        "config_hash":      dbResult.rows[i].CONFIG_HASH
                    }
                );
            }

            resolve(busObj);
        });
    }

}