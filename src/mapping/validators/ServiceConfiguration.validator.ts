import Joi                          from "joi"
import AppConfig                    from "../../config/AppConfig";
import  logger                      from "../../utils/Logger";
import {PostRespBusinessObjects}    from "../bussObj/Response";



export class ServiceConfigurationValidator{
    private readonly _MAX_INREQ:number  = 100;

    RequestQueryParamSchema = Joi.object({
        "serviceID" : Joi.string().min(8).max(56).optional(),
        "versionID" : Joi.string().min(8).max(56).required(),
        "serviceName": Joi.string().min(8).max(64).optional(),
        "configKey" : Joi.string().min(2).max(56).optional(),
    });

    ServiceConfigurationBO = Joi.object({
        "serviceconfiguration": {
            "service_name": Joi.string().min(2).max(128).required(),
            "service_id": Joi.string().min(8).max(56).required(),
            "version_id": Joi.string().min(8).max(56).required(),      
            "config": Joi.array().items(
                {
                    "config_key": Joi.string().min(2).max(128).required(),      
                    "config_key_desc":  Joi.string().min(8).max(512).required(),
                    "config_value":     Joi.string().min(1).max(512).required(),
                    "config_type":      Joi.string().min(3).max(56).required()  
                }
            ).required().min(1).max(this._MAX_INREQ)
        }
    });


    /**
     * Validates query parameter and throws exception in-case of an invalid param
     * @param {string} queryParam Query parameter to validate
     */
    public validateQueryParams(queryParam:any){
        let {error, value} = this.RequestQueryParamSchema.validate(queryParam, { presence: "required" });
        if(error){
            logger.error(`Validation of query parameter failed ${error}`);                    
            this.getValidationError(error);
        }else{
            return value;
        }
    }

    /**
     * Validates business object for create / update
     * @param request Request business object to validate
     */
    public validateForCreate(request:any){
        logger.debug(`Validator called to validate: ${request}`);
        let {error, value} = this.ServiceConfigurationBO.validate(request, { presence: "required" });
        if(error){
            logger.error(`Validation of the business object ${error.message}`);                    
            this.getValidationError(error);
        }else{
            return value;
        }
    }

    private getValidationError(error:any){
        if(AppConfig.config.OFL_MED_NODE_ENV === 'debug'){
            let trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8502","Schema validation error", trace);   
        }else{
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8502","Schema validation error");   
        }           
    }
}