/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express                              from "express";
import { ServiceConfigurationValidator }    from "../mapping/validators/ServiceConfiguration.validator";
import {AppHealth}                          from "../config/AppConfig";
import logger                               from "../utils/Logger";
import {PostRespBusinessObjects}            from "../mapping/bussObj/Response";
import util                                 from "util";
import { ServiceConfigurationController }   from "../mapping/ServiceConfigController";

const router: any = express.Router();

/**
 * GET All service convifgurations from the db based on service_id and service_version
 * ?serviceID
 * ?versionID
 */
router.get('/', async (request: any,response: any)=> {    
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for getting service-configuration`);    
    try{
        let srvConfigValidator:ServiceConfigurationValidator = new ServiceConfigurationValidator();
        let queryValue = srvConfigValidator.validateQueryParams({"serviceID": request.query.serviceID, 
                                                                   "versionID": request.query.versionID,
                                                                    "serviceName": request.query.serviceName});
        logger.debug(`Query parameters parsed: ${queryValue}`);
        let srvConfigs = await new ServiceConfigurationController().getAll(queryValue.serviceID,queryValue.versionID, queryValue.serviceName);
        response.status(200).send(srvConfigs);
    }catch(error){
        logger.debug(`Error occured while fetching all service-master  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(getErrorCodes(error));
        if(error.metadata !== undefined){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        } 
    }    
});

/**
 * GET All service convifgurations from the db based on service_id and service_version
 * ?serviceID
 * ?versionID
 */
router.get('/slim', async (request: any,response: any)=> {    
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for getting service-configuration`);    
    try{
        let srvConfigValidator:ServiceConfigurationValidator = new ServiceConfigurationValidator();
        let queryValue = srvConfigValidator.validateQueryParams({"serviceID": request.query.serviceID, 
                                                                   "versionID": request.query.versionID,
                                                                    "serviceName": request.query.serviceName});
        logger.debug(`Query parameters parsed: ${queryValue}`);
        let srvConfigs = await new ServiceConfigurationController().getSlim(queryValue.serviceID,queryValue.versionID, queryValue.serviceName);
        response.status(200).send(srvConfigs);
    }catch(error){
        logger.debug(`Error occured while fetching all service-master  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(getErrorCodes(error));
        if(error.metadata !== undefined){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        } 
    }    
});

router.post('/',async (request: any,response: any)=> {    
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for Creating service-configuration`);
    try{
        // Step#1: Validate the request object
        let srvConfigValidator:ServiceConfigurationValidator = new ServiceConfigurationValidator();
        let parsedObj = srvConfigValidator.validateForCreate(request.body);
        logger.debug(`Parsed response from validation: ${util.inspect(parsedObj,{compact:true,colors:true, depth: null})}`);
        // Step#2: Call Service controller for creating the service-configuration
        let createResp = await new ServiceConfigurationController().create(parsedObj);
        response.status(201).send(createResp);   
    }catch(error){
        logger.debug(`Error occured while fetching all service-master  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(getErrorCodes(error));
        if(error.metadata !== undefined){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        } 
    }    
});

router.put('/',async (request: any,response: any)=> {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for Deleting service-configuration`);
    try{
        // Step#1: Validate the request object
        let srvConfigValidator:ServiceConfigurationValidator = new ServiceConfigurationValidator();
        let parsedObj = srvConfigValidator.validateForCreate(request.body);
        logger.debug(`Parsed response from validation: ${util.inspect(parsedObj,{compact:true,colors:true, depth: null})}`);
        // Step#2: Call Service controller for creating the service-configuration
        let createResp = await new ServiceConfigurationController().update(parsedObj);
        response.status(201).send(createResp);   
    }catch(error){
        logger.debug(`Error occured while fetching all service-master  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(getErrorCodes(error));
        if(error.metadata !== undefined){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        } 
    }    
});

router.delete('/',async (request: any,response: any)=> {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for getting service-configuration`); 
    try{        
        let queryValue = new ServiceConfigurationValidator().validateQueryParams({"serviceID": request.query.serviceID, 
                                                                                    "versionID": request.query.versionID,
                                                                                        "configKey": request.query.configKey});
        logger.debug(`Query parameters parsed: ${queryValue}`);        
        let srvConfigs = await new ServiceConfigurationController().delete(queryValue.serviceID,queryValue.versionID,queryValue.configKey);
        response.status(200).send(srvConfigs);
    }catch(error){
        logger.debug(`Error occured while fetching all service-master  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(getErrorCodes(error));
        if(error.metadata !== undefined){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        } 
    }        
});


function getErrorCodes(error:any):number{
    if(error.metadata === undefined){
        return 500;
    }else if(error.metadata.status === '8501'){
        return 404;         
    }else{
        return 500;
    }
}


export default router;