/**
  * @copyright Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Part of the framework
 * @description
 * - Stalk test cases for testing framework components
 * @author Muhammad Furqan <furqan.baqai@openfintechlab.com>
 * 
 */

import AppConfig    from "../../config/AppConfig";
import StaticVars   from "../../config/StaticVars";


describe(`Config Management Test Cases`, () => {
    const package_json = require(`../../../package.json`);

    beforeAll( ()=> {
        // Load configurations
        let loaded:boolean = AppConfig.validateConfig();
        expect(loaded).toBeTruthy();
    });

    test('Configuration from OS environment is getting loaded', ()=> {        
        expect(AppConfig.config.PATH).toBeDefined();
        expect(AppConfig.config.PATH === process.env.PATH).toBeTruthy();
    });

    test('Loads context root from environment variable', () => {
        process.env[StaticVars.ConfigVars.contextRoot] = '/test123';
        expect(AppConfig.contextRoot).toBe('/test123');
        process.env[StaticVars.ConfigVars.contextRoot] = '';
    });    
    
    test('Loads context root from package.json', ()=> {        
        let ctxRoot = AppConfig.contextRoot;
        expect(ctxRoot).toBe("/" + package_json.name);
    });
    

});